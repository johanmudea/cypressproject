describe('US #: esto es un caso de prueba real',()=>{
    beforeEach('precondicion de ir a la pagina y llegar al formulario',()=>{

        cy.visit('https://demo.testim.io/')

        cy.fixture("login_fixture").then((the)=>{
            cy.contains(the.loginButton.On).click()
            cy.url().should("contain","login")

        })    

    })

    it('US # 2 | TC # 2: validar inicion exitoso del formulario con fixture ',()=>
        {
            cy.fixture("login_fixture").then((the)=>{


                cy.get(the.username.input).type(the.username.data.valid)
                cy.get(the.password.input).type(the.password.data.valid)
                cy.get(the.SubmitButton).click()
                cy.contains(the.loginButton.Off).should("be.visible")




            })
            

    })
    it('US # 3 | TC # 3: validar inicion exitoso del formulario con fixture y con clave mala',()=>
        {
            cy.fixture("login_fixture").then((the)=>{


                cy.get(the.username.input).type(the.username.data.valid)
                cy.get(the.password.input).type(the.password.data.invalid)
                cy.get(the.SubmitButton).click()
                cy.contains(the.loginButton.Off).should("not.to.be.visible")




            })
            

    })


})