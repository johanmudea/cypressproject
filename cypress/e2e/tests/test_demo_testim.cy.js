describe('US #: esto es un caso de prueba real',()=>{
    beforeEach('precondicion de ir a la pagina y llegar al formulario',()=>{
        cy.visit('https://demo.testim.io/')
        cy.contains('Log in').click()
        cy.url().should("contain","login")

    })

    it('US # 1 | TC # 1 : validar inicion exitoso del formulario ',()=>
        {

            cy.get("[tabindex='1']")
            .type("John")

            cy.get("[tabindex='2']")
            .type("Admin123.")

            cy.get("[tabindex='3']")
            .click()

            cy.contains("Hello, John").should("be.visible")
    })


    it('US # 1 | TC # 1 : validar inicion con BR especial characts faltante ',()=>
        {

            cy.get("[tabindex='1']")
            .type("John")

            cy.get("[tabindex='2']")
            .type("Admin123")

            cy.get("[tabindex='3']")
            .click()

            cy.contains("Hello, John").should("not.to.be.visible")
    })


})